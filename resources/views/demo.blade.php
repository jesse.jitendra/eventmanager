<!DOCTYPE html>
<html>
<head>
	<title>Demo page</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css')  }}">
	 <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>




<div class="container">
	 <div class="row">
	 	<div class="col">
	 		 <div id="app">
                     <example-component></example-component>
                </div>
	 	</div>
	 	<div class="col"></div>
	 </div>
</div>


  

<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
