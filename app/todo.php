<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class todo extends Model
{
    //
    protected $fillable = ['name'];
    protected $table = "Todo";
    protected $primarykey = "id";
}
